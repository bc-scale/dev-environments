# Build our devenv from our custom workspace image
FROM quay.io/gitpodified-workspace-images/full:latest

# Infra Toolkit and 'thefuck' pip package
# batcat and yq will be moved to the main full image.
RUN brew install kubectl helm terraform okteto openshift-cli \
    && sudo sh -c "$(curl -sSL https://raw.githubusercontent.com/railwayapp/cli/master/install.sh)"

# get our base zshrc
COPY docker/base.zshrc /home/gitpod/.zshrc
# and also our bashrc.d ports for zsh
COPY docker/.zshrc.d /home/gitpod/.zshrc.d
# Copy our customized zshrc there
COPY dotfiles/zshrc/ /home/gitpod/.gitpodify/custom-zshrc.d/
# Add our scripts to the image, ensuring our gp-devenv script is fresh as possible.
COPY scripts/ /home/gitpod/.local/bin

# Fix file ownership issues
RUN sudo chown -Rv gitpod:gitpod /home/gitpod/.gitpodify/custom-zshrc.d

# Open Zsh shell as login shell by default
CMD [ "/usr/bin/zsh", "-l" ]